﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickMove : MonoBehaviour {

    public int moveSpeed = 3;
    public 


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonUp(0)) {
            Debug.Log("Mouse 0 detected");
            Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.Log("ClickRay: " + clickRay.origin + " -> " + clickRay.direction);
            //RaycastHit hit;
            Vector3 currentPoint = transform.position;
            Debug.Log("CurrentPoint: " + currentPoint);
            float rotateAngle = Vector3.Angle(currentPoint, clickRay.direction);
            Debug.Log("Angle: " + rotateAngle);
            transform.LookAt(clickRay.direction);
        }
	}
}
